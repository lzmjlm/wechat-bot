package com.github.maojx0630.wechat.bot.dto;

import lombok.Data;

/**
 * @author 毛家兴
 * @since 2024-04-17 14:22
 */
@Data
public class ImageParam {

  private String wxid;

  private String path;

  private String image;
}
