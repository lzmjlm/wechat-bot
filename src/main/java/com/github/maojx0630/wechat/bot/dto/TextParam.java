package com.github.maojx0630.wechat.bot.dto;

import java.util.List;
import lombok.Data;

/**
 * @author 毛家兴
 * @since 2024-04-17 14:22
 */
@Data
public class TextParam {

  private String wxid;

  private String content;

  private List<String> atlist;
}
