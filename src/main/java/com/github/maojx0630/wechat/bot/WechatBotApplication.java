package com.github.maojx0630.wechat.bot;

import com.github.maojx0630.wechat.bot.monitor.WechatUtil;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

/**
 * @author 毛家兴
 * @since 2024-04-16 11:35
 */
@SpringBootApplication
public class WechatBotApplication {
  public static void main(String[] args) {
    ConfigurableApplicationContext run = SpringApplication.run(WechatBotApplication.class, args);
    run.getBean(WechatUtil.class).userInfo();
  }
}
