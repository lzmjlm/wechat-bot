package com.github.maojx0630.wechat.bot.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * @author 毛家兴
 * @since 2024-04-17 11:18
 */
@Data
@Configuration
@ConfigurationProperties(prefix = "bot")
public class WechatConfig {

  /** 要反向连接到的websocket地址 */
  private String ws;

  /** 反向时的token */
  private String token;

  /** 微信接口地址 */
  private String wechat;
}
