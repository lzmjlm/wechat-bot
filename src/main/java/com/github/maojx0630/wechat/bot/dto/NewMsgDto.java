package com.github.maojx0630.wechat.bot.dto;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @author 毛家兴
 * @since 2024-04-16 11:56
 **/
@Data
public class NewMsgDto implements Serializable {


  /**
   * data : [{"BytesExtra":"CgQIEBAAGhEIARINcGlhb2d1bzg4NDM0MBrOAggHEskCPG1zZ3NvdXJjZT4KICAgIDxiaXpmbGFnPjA8L2JpemZsYWc+CiAgICA8cHVhPjE8L3B1YT4KICAgIDxzaWxlbmNlPjA8L3NpbGVuY2U+CiAgICA8bWVtYmVyY291bnQ+MjwvbWVtYmVyY291bnQ+CiAgICA8c2lnbmF0dXJlPlYxX3cxKzM3NzEwfHYxX3cxKzM3NzEwPC9zaWduYXR1cmU+CiAgICA8dG1wX25vZGU+CiAgICAgICAgPHB1Ymxpc2hlci1pZCAvPgogICAgPC90bXBfbm9kZT4KICAgIDxzZWNfbXNnX25vZGU+CiAgICAgICAgPGFsbm9kZT4KICAgICAgICAgICAgPGZyPjE8L2ZyPgogICAgICAgIDwvYWxub2RlPgogICAgPC9zZWNfbXNnX25vZGU+CjwvbXNnc291cmNlPgoaJAgCEiA2ZTRlNWI4MWJjMzdiZDQyZTY5MDJhZjg2ZTEyYWMxMg==","BytesTrans":"","CompressContent":"","CreateTime":"1713239756","DisplayContent":"","FlagEx":"0","IsSender":"0","MsgSequence":"795970165","MsgServerSeq":"1","MsgSvrID":"1538317697375722149","Reserved0":"0","Reserved1":"2","Reserved2":"","Reserved3":"","Reserved4":"","Reserved5":"","Reserved6":"","Sender":"piaoguo884340","Sequence":"1713239756000","Status":"2","StatusEx":"0","StrContent":"123","StrTalker":"38974800713@chatroom","SubType":"0","TalkerId":"4","Type":"1","localId":"12"}]
   * total : 1
   * wxid : wxid_d2uirrwv9e519
   */

  private int total;
  private String wxid;
  private List<DataBean> data;

  @Data
  public static class DataBean implements Serializable {

    private String BytesExtra;
    private String BytesTrans;
    private String CompressContent;
    private String CreateTime;
    private String DisplayContent;
    private String FlagEx;
    private String IsSender;
    private String MsgSequence;
    private String MsgServerSeq;
    private String MsgSvrID;
    private String Reserved0;
    private String Reserved1;
    private String Reserved2;
    private String Reserved3;
    private String Reserved4;
    private String Reserved5;
    private String Reserved6;
    /**
     * 群聊人wxid
     */
    private String Sender;
    private String Sequence;
    private String Status;
    private String StatusEx;
    /**
     * 消息内容
     */
    private String StrContent;
    /**
     * 消息发送者微信ID
     */
    private String StrTalker;
    private String SubType;
    private String TalkerId;
    private String Type;
    private String localId;
  }
}
